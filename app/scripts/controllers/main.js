'use strict';

/**
 * @ngdoc function
 * @name mongomapApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mongomapApp
 */
angular.module('mongomapApp')
  .controller('MainCtrl', function ($scope,$http, $log) {
    this.showMap = true;
    this.mapOptions = {
      center: new google.maps.LatLng(52.1, 5.247765),
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this.myMap;
    this.markers = [];
    var self = this;

    this.onSubmit = function(query){
      var options = {};
      self.markers.forEach(function(marker){
        marker.setMap(null);
      });
      self.markers.length = 0;
      var url = "http://geo3d.herokuapp.com/api/cities/"+query.city;
      options.params = {
          radius : 0,
          maxpop : query.maxpop,
          minpop : query.minpop
      };
      if (query.radius) {
        options.params.radius = query.radius;
      }
      self.showMap = false;
      $http.get(url,options).then(function onSuccess(response){
        var data = response.data;
        self.showMap = true;
        if (angular.isArray(data)){
          data.forEach(function(marker){
            processMarker(marker);
          });
        } else {
          processMarker(data);
        }

      });
    }

    function processMarker(data){
     self.markers.push( new google.maps.Marker({
        map: self.myMap,
        position: new google.maps.LatLng(data.location.coordinates[1],data.location.coordinates[0]),
        animation: google.maps.Animation.DROP,
        title: data.asciiname + " : "+ data.population
      }));
    }


  });
