'use strict';

/**
 * @ngdoc overview
 * @name mongomapApp
 * @description
 * # mongomapApp
 *
 * Main module of the application.
 */
angular
  .module('mongomapApp', [
    'ngResource','ui.map','ui.event'
  ]);

